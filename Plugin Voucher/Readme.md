## Basic Information

A SQL script is a set of SQL commands saved as a file in SQL Scripts.


## Links for all scripts

GLPI v 9.5.5 - https://glpi-project.org/

Credit Plugin - https://github.com/pluginsGLPI/credit

## License

GNU General Public License (GPL-2.0).

## Contact to author

Krystian Rożewicz 

GLPI@dev.epc24.pl

www.epc24.pl
