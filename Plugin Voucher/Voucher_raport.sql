************************************************
* Raport Voucher consumed at last month        *
************************************************
* File Name: Voucher_report.sql                *
* Created Date: 2021-08-08                     *
* Modyfication Date: 2021-08-08                *
* Created by: Krystian Rożewicz (ePC24.pl)     *
************************************************
* Log                                          *
* 2021-08-08 23:00                             *
* First version of SQL script                  *
************************************************

SELECT 
	DISTINCT tickets.id as "Ticket Number",
	tickets.name as "Ticket Name", 
	tickets.date as "Ticket Start", 
	IFNULL(DATE_FORMAT(tickets.solvedate,'%Y-%m-%d'), "No") as "Ticket Solve Date",
	IFNULL(DATE_FORMAT(tickets.closedate,'%Y-%m-%d'), "No")  as "Ticket Close Date",
	CASE
	    WHEN tickets.priority = 5 THEN "Very high"
	    WHEN tickets.priority = 4 THEN "High"
	    WHEN tickets.priority = 3 THEN "Medium"
	    WHEN tickets.priority = 2 THEN "Low"
	    WHEN tickets.priority = 1 THEN "Very low"
	    ELSE "Unknown"
	END as "Ticket Priority",
	ent.name as "Client Name",
	IFNULL((sum(DISTINCT tc.cost_fixed)+sum(DISTINCT tc.cost_material)+sum(DISTINCT tc.cost_time)), 0) as "Ticket Cost",
	IFNULL(sum(DISTINCT credit.consumed), 0) as "fff",
	DATE_FORMAT((CURRENT_DATE() - INTERVAL 1 MONTH),'%Y-%m') as "Raport Year-Month"
FROM glpi_tickets as tickets
left join glpi_plugin_credit_tickets as credit on credit.tickets_id = tickets.id
left join glpi_entities ent on tickets.entities_id = ent.id 
left join glpi_ticketcosts tc on tickets.id = tc.tickets_id 
where 
	DATE_FORMAT(tickets.solvedate,'%Y-%m')=DATE_FORMAT((CURRENT_DATE() - INTERVAL 1 MONTH),'%Y-%m') or
	tickets.solvedate is NULL or
	tickets.closedate is NULL
GROUP by 
	tickets.id
order by  
	(sum(DISTINCT tc.cost_fixed)+sum(DISTINCT tc.cost_material)+sum(DISTINCT tc.cost_time)) DESC, 
	tickets.solvedate DESC; 